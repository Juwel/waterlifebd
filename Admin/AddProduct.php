
<?php
require_once("../../../vendor/autoload.php");

use WaterLife\Message\Message;

/*
 * if(!isset($_SESSION)){
    session_start();
}
*/ //session will add when needed
$msg = Message::getMessage();

echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add New Product</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<body>

<div class="container" style='height: 50px'>

    <div class="navbar">

        <td><a href='' class='btn btn-group-lg btn-info'>Product List</a> </td>

    </div>



    <form class="form-group" action="ProductStoreToDatabase.php" method="post" enctype="multipart/form-data">

        <b>Product Name :</b>
        <input class="form-control" type="text" name="productName" required>
        <br>
        <b>Product Code :</b>
        <input class="form-control" type="text" name="productCode" required>
        <br>
        <b>Price :</b>
        <input class="form-control" type="number" name="productPrice" required>
        <br>
        <b>Product Catagory:<br></b>
        <input type="radio" name = productCaragory value="Domestic" required>Domestic
        <br>
        <input type="radio"  name = productCaragory value="Industrial" required>Industrial
        <br>
        <input type="radio"  name = productCaragory value="Commercial" required>Commercial
        <br>
        <input type="radio"  name = productCaragory value="Swming" required>Textile
        <br>
        <input type="radio"  name = productCaragory value="Oil Refinery" required>Oil Refinery
        <br>
        <input type="radio"  name = productCaragory value="Garments" required>Garments
        <br>
        <br>
        <b>Product Image:</b><br>
        <input type="file" name="image" accept="image/*" required>
        <br>
        <br><br>
        <input type="submit" class="btn btn-primary" value="Add Product To Web Page">

    </form>

</div>

</body>

</html>
